from flask import Flask, render_template, request, redirect
from forms import SignUpForm 

app = Flask(__name__)
app.config['SECRET_KEY'] = "ALongAndDifficultKeyToBeVerySafeOk?"

@app.route('/')
def home():
    return "Hello World"

@app.route('/about')
def about():
    return "The about page"

@app.route('/blog')
def blog():
    posts = [{'title':'tech in 2023','author':'rafa'},
             {'title':'war on palestine', 'author':'bob'}]
    return render_template('blog.html', author='Test', sunny=True, posts=posts)

@app.route('/blog/<blog_id>')
def blogpost(blog_id):
    return f"This is the blog post number {blog_id}"

@app.route('/signup', methods=['GET','POST'])
def signup(): 
    form = SignUpForm()
    if form.is_submitted():
        result = request.form
        return render_template("user.html", result=result)
    return render_template("signup.html", form=form)

if __name__ == "__main__":
    app.run(debug=True)
